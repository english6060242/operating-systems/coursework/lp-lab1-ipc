# IPC Project: Multi-Protocol Client-Server Architecture

This project implements a multi-protocol client-server architecture using various IPC mechanisms such as sockets. The objective is to design, implement, and test applications in C that leverage IPC mechanisms provided by the Operating System.

## Project Overview

![Block Diagram](img/block_diagram.jpg)

### Objective
The project aims to implement a client-server architecture with at least 3 protocols from the <sys/socket.h> family. The server should handle multiple clients across various protocols, logging data counts per protocol, and the total received data to persistent files.

### Development Details
The implementation includes three pairs of client/server programs initially for different socket configurations: INET (IPV4), UNIX, and INET6 (IPV6). These servers will later be merged into a single server capable of handling all protocols.

### Thread Structure
The server launches dedicated threads for accepting connections for each protocol (Server_INET_Stream, Server_UNIX_stream, Server_INET6_stream). For each connection, a TaskHandlingThread manages data reception and accumulation per protocol, while a CountingThread ensures thread lifecycle management.

### Key Features
- Dynamic handling of multiple clients across INET, UNIX, and INET6 protocols.
- Accurate logging and persistence of received data counts.
- Thread-based architecture ensuring efficient handling of connections and data accumulation.

## Usage Instructions
- Compile and run the server side code (make testServer).
- Comppile the client side code.
- The client side Makefile includes individual tests for all the client types or a global test using a bash file (manage permissions before using that option). 

### Getting Started
To initialize the project:
- Clone this repository.
- Compile the code using the provided compilation flags.
- Supply necessary parameters to the server to specify addresses, ports, etc.

### Collaborate and Test
- Invite team members and collaborators using GitLab's collaboration features.
- Create merge requests, enable approvals, and set up protected environments for testing.

### CI/CD Integration
Utilize GitLab's CI/CD capabilities for automated testing and code analysis. Set up deployments for Kubernetes, Amazon EC2, or ECS.

### Project Status
This project is actively maintained and welcomes contributions. Please refer to CONTRIBUTING.md for contribution guidelines.

## Installation and Dependencies
Ensure the following dependencies are met before running the code:
- C Compiler
- <sys/socket.h> libraries

## License
This project is licensed under the [MIT License](LICENSE.md).