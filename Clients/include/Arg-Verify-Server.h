#ifndef SERVERARGVERIFY_H   
#define SERVERARGVERIFY_H_H

void verifyArguments(int argc, char *argv[]);
void verifyArgumentsINET(char *argv[]);
void verifyArgumentsUNIX(char *argv[]);
void verifyArgumentsINET6(char *argv[]);

#endif 