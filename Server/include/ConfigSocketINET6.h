#ifndef INET6SOCKETCONFIG_H  
#define INET6SOCKETCONFIG_H_H

void ServerConfigSocketINET6(int *sock, struct sockaddr_in6 *servaddr, int iport, long unsigned int max, char *stringaddr, char *interfaceName);

#endif 