#ifndef SERVERARGVERIFY_H    // Start of the #ifndef guard
#define SERVERARGVERIFY_H    // Header guard

void verifyArguments(int argc, char *argv[]);
void verifyArgumentsINET(char *argv[]);
void verifyArgumentsUNIX(char *argv[]);
void verifyArgumentsINET6(char *argv[]);

#endif // End of the #ifndef guard
